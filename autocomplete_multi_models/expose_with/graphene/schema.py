import django_filters
import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

import autocomplete_multi_models.business_process
import autocomplete_multi_models.models


class IndexedWordFilter(django_filters.FilterSet):
    q = django_filters.CharFilter(method='resolve_q')

    class Meta:
        model = autocomplete_multi_models.models.IndexedWord
        exclude = ('id',)

    def resolve_q(self, queryset, name, value, *args, **kwargs):
        return autocomplete_multi_models.business_process.get_closest_matching_words(value)


class IndexedWordNode(DjangoObjectType):
    similarity = graphene.Field(graphene.Float)

    class Meta:
        model = autocomplete_multi_models.models.IndexedWord
        interfaces = (graphene.Node,)


class FindClosestWordsQuery(graphene.ObjectType):

    autocomplete = DjangoFilterConnectionField(IndexedWordNode, filterset_class=IndexedWordFilter)
