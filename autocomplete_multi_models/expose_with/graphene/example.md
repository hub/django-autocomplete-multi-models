```
{
  autocomplete(q: "Homer") {
    edges {
      node {
        word,
        similarity
      }
    }
  }
}
```

returns

```json
{
  "data": {
    "autocomplete": {
      "edges": [
        {
          "node": {
            "word": "home",
            "similarity": 0.5714286
          }
        },
        {
          "node": {
            "word": "homodimer",
            "similarity": 0.45454547
          }
        },
        {
          "node": {
            "word": "homo",
            "similarity": 0.375
          }
        },
        {
          "node": {
            "word": "homemade",
            "similarity": 0.36363637
          }
        },
        {
          "node": {
            "word": "HMMER",
            "similarity": 0.33333334
          }
        },
        {
          "node": {
            "word": "homme",
            "similarity": 0.33333334
          }
        },
        {
          "node": {
            "word": "homeostatic",
            "similarity": 0.2857143
          }
        },
        {
          "node": {
            "word": "homeodomain",
            "similarity": 0.2857143
          }
        },
        {
          "node": {
            "word": "homeostasis",
            "similarity": 0.2857143
          }
        },
        {
          "node": {
            "word": "monomer",
            "similarity": 0.27272728
          }
        }
      ]
    }
  }
}
```