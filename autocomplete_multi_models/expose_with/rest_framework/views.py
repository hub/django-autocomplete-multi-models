from rest_framework import views, response, status

from autocomplete_multi_models import business_process
from autocomplete_multi_models.expose_with.rest_framework import serializers


class FindClosestWords(views.APIView):
    def get(self, request, format=None):
        return self.do_the_work(request.GET)

    def post(self, request, format=None):
        return self.do_the_work(request.data)

    def do_the_work(self, data):
        # {"q":"rna","min_similarity":0.1,"limit":8}
        search_serializer = serializers.SearchSerializer(data=data)
        if not search_serializer.is_valid():
            return response.Response(search_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        results_serializer = serializers.IndexedWordSerializer(
            business_process.get_closest_matching_words(
                search_serializer.data['q'],
                search_serializer.data['limit'],
                search_serializer.data['min_similarity'],
            ),
            many=True,
        )
        return response.Response(results_serializer.data)
