from django.urls import path

from autocomplete_multi_models.expose_with.rest_framework.views import FindClosestWords

urlpatterns = [
    path('autocomplete/', FindClosestWords.as_view()),
]
