from rest_framework import serializers

from autocomplete_multi_models import models


class IndexedWordSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.IndexedWord
        fields = [
            'word',
            'similarity',
        ]

    similarity = serializers.FloatField()


class SearchSerializer(serializers.Serializer):
    q = serializers.CharField(
        min_length=2,
        required=True,
    )
    limit = serializers.IntegerField(
        min_value=1,
        default=10,
        required=False,
    )
    min_similarity = serializers.FloatField(
        required=False,
        default=None,
    )
