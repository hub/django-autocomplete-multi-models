import time
from django.core.management import BaseCommand
from django.db import transaction

from autocomplete_multi_models import business_process, models, utils


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--forced', action='store_true')
        parser.add_argument('--no-fill-if-empty', action='store_true')

    @transaction.atomic
    def handle(self, *args, **options):
        if not (options['forced'] or business_process.get_setting_from_storage(utils.REBUILD_NEEDED, True)):
            return

        ts = time.time()
        business_process.rebuild_index()
        te = time.time()
        print(f"Index rebuild in {int((te - ts) * 100) / 100}s, it contains {models.IndexedWord.objects.count()} words")
        if not options['no_fill_if_empty'] and not models.IndexedWord.objects.exists():
            for s in [
                "Coming soon",
                "coming soon",
                "coming",
                "soon",
                "Work in progress",
                "work",
                "progress",
                "HUB",
                "Bioinformatics and Biostatistics",
                "Bioinformatics and Biostatistics HUB",
                "bioinformatics",
                "biostatistics",
            ]:
                models.IndexedWord.objects.get_or_create(word=s, occurrence=1)
        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, False)
