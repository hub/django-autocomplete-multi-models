from django.contrib import admin
from django.db import connection

# Register your models here.
from autocomplete_multi_models import models


@admin.register(models.IndexedWord)
class IndexedWordAdmin(admin.ModelAdmin):
    list_display = ("word", "occurrence")
    search_fields = ("word",)
    actions = [
        "ban_from_index",
    ]

    def ban_from_index(self, request, queryset):
        to_add = []
        with connection.cursor() as cursor:
            for o in queryset:
                cursor.execute("SELECT UPPER(UNACCENT(%s)) as value", [o.word])
                word = cursor.fetchone()[0]
                if not models.BannedWordFromIndex.objects.filter(word=word).exists():
                    to_add.append(models.BannedWordFromIndex(word=word))
                o.delete()
        models.BannedWordFromIndex.objects.bulk_create(to_add)


@admin.register(models.BannedWordFromIndex)
class BannedWordFromIndexAdmin(admin.ModelAdmin):
    list_display = ("word",)
    search_fields = ("word",)
