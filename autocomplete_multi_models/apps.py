from functools import partial

from django.apps import AppConfig
from django.db.models import signals

from autocomplete_multi_models import utils


class AutocompleteMultiModelsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'autocomplete_multi_models'

    __indexed_fields = None

    def ready(self):
        from . import signals as my_signals

        utils.init_from_settings()

        for model, field_names in utils.get_indexed_fields().items():
            sender = model
            signals.post_save.connect(partial(my_signals.instance_update, field_names=field_names), sender=sender)
            signals.pre_delete.connect(partial(my_signals.instance_delete, field_names=field_names), sender=sender)
