import django.contrib.postgres.indexes
import django.db.models
from django.contrib.postgres.lookups import Unaccent
from django.db.models import F
from django.db.models.functions import Upper
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.
class IndexedWord(django.db.models.Model):
    indexes = [
        django.contrib.postgres.indexes.GinIndex(fields=['word']),
    ]
    word = django.db.models.CharField(
        max_length=64,
        db_index=True,
        null=False,
    )
    occurrence = django.db.models.IntegerField(
        null=True,
        blank=True,
        default=None,
    )

    def __str__(self):
        return self.word


class BannedWordFromIndex(django.db.models.Model):
    class Meta:
        verbose_name_plural = "Banned words from index"

    indexes = [
        django.contrib.postgres.indexes.GinIndex(fields=['word']),
    ]
    word = django.db.models.CharField(
        max_length=64,
        db_index=True,
        unique=True,
        null=False,
    )

    def __str__(self):
        return self.word


@receiver(post_save, sender=BannedWordFromIndex)
def flush_live_settings_in_cache(instance, *args, **kwargs):
    BannedWordFromIndex.objects.filter(id=instance.id).update(word=Unaccent(Upper(F('word'))))
