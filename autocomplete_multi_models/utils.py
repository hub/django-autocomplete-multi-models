from django.conf import settings
from django.utils.module_loading import import_string
from django.apps import apps

DEFAULT_AUTOCOMPLETE_MIN_LENGTH = 4
DEFAULT_AUTOCOMPLETE_MIN_SIMILARITY = 0.3
DEFAULT_AUTOCOMPLETE_LIMIT = 10
DEFAULT_SHOULD_MERGE_PLURAL_INTO_SINGULAR = False
REBUILD_NEEDED = "is_autocomplete_multi_models_rebuild_needed"
DEFAULT_CAN_BE_INDEXED_BY_AUTOCOMPLETE_FUNCTION_NAME = "can_be_indexed_by_autocomplete"
AUTO_UPDATE_ENABLED = "is_autocomplete_auto_update_on_save_enabled"


def get_indexed_fields():
    __indexed_fields = dict()
    for app_name, model_name, field_name in settings.AUTOCOMPLETE_TARGET_FIELDS:
        __indexed_fields.setdefault(apps.get_model(app_name, model_name), []).append(field_name)
    return __indexed_fields


def init_from_settings():
    from . import business_process

    business_process._AUTOCOMPLETE_MIN_LENGTH = getattr(
        settings,
        'AUTOCOMPLETE_MIN_LENGTH',
        DEFAULT_AUTOCOMPLETE_MIN_LENGTH,
    )
    business_process._AUTOCOMPLETE_MIN_SIMILARITY = getattr(
        settings,
        'AUTOCOMPLETE_MIN_SIMILARITY',
        DEFAULT_AUTOCOMPLETE_MIN_SIMILARITY,
    )
    business_process._AUTOCOMPLETE_LIMIT = getattr(
        settings,
        'AUTOCOMPLETE_LIMIT',
        DEFAULT_AUTOCOMPLETE_LIMIT,
    )
    business_process._SHOULD_MERGE_PLURAL_INTO_SINGULAR = getattr(
        settings,
        'SHOULD_MERGE_PLURAL_INTO_SINGULAR',
        DEFAULT_SHOULD_MERGE_PLURAL_INTO_SINGULAR,
    )
    business_process._CAN_BE_INDEXED_BY_AUTOCOMPLETE_FUNCTION_NAME = getattr(
        settings,
        'CAN_BE_INDEXED_BY_AUTOCOMPLETE_FUNCTION_NAME',
        DEFAULT_CAN_BE_INDEXED_BY_AUTOCOMPLETE_FUNCTION_NAME,
    )
    get_set_names = getattr(settings, 'AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER', None)
    if get_set_names is not None:
        get_fcn = import_string(get_set_names[0])
        set_fcn = import_string(get_set_names[1])

        set_fcn("autocomplete_multi_models_test_settings", "1")
        assert get_fcn("autocomplete_multi_models_test_settings", "3") == "1"
        set_fcn("autocomplete_multi_models_test_settings", "2")
        assert get_fcn("autocomplete_multi_models_test_settings", "3") == "2"

        set_fcn("autocomplete_multi_models_test_settings", None)  # cleanup

        business_process.get_setting_from_storage = get_fcn
        business_process.set_setting_in_storage = set_fcn


# def get_min_length():
#     return
#
#
# def get_min_similarity():
#     return
#
#
# def get_limit():
#     return
#
#
# def get_get_key_in_persistent_setting_storage():
#     def fcn(key):
#         return key == REBUILD_NEEDED
#
#     return fcn
#
#
# def get_set_key_in_persistent_setting_storage():
#     def fcn(key, value):
#         pass
#
#     return fcn
