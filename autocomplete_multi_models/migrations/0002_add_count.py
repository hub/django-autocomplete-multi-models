from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('autocomplete_multi_models', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='IndexedWord',
            name='occurrence',
            field=models.IntegerField(default=None, null=True, blank=True),
        ),
    ]
