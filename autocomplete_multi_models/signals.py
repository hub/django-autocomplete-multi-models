from autocomplete_multi_models import business_process, utils


def instance_update(sender, instance, field_names, **kwargs):
    if business_process.get_setting_from_storage(utils.AUTO_UPDATE_ENABLED, "True") == "True":
        business_process.add_instance_to_index(instance, field_names)
    else:
        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, True)


def instance_delete(sender, instance, field_names, **kwargs):
    business_process.set_setting_in_storage(utils.REBUILD_NEEDED, True)
