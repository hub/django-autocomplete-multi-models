import json


def get_data():
    try:
        with open('/tmp/autocomplete_multi_models.json', 'r') as f:
            return json.load(f)
    except OSError:
        return dict()


def save_data(data):
    with open('/tmp/autocomplete_multi_models.json', 'w') as f:
        return json.dump(data, f, indent=4)


def get_fcn(key, default):
    return get_data().get(key, default)


def set_fcn(key, value):
    data = get_data()
    data[key] = value
    save_data(data)
