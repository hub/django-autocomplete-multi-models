from django.db import connection
from django.db import models
from django.test import TestCase

from autocomplete_multi_models import utils


class ChangeAutoCompleteSettingsTestCase(TestCase):
    class Meta:
        abstract = True

    def setUp(self) -> None:
        utils.init_from_settings()  # needed in test as app in only initialized once for test


class WithTextModel(models.Model):
    text = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'unmanaged_with_text_table'

    def can_be_indexed_by_autocomplete(self):
        return self.text != "NONONO"

    def __str__(self):
        return self.text


class WithTextModelTestCase(TestCase):
    def setUp(self):
        super().setUp()

        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(WithTextModel)

            if WithTextModel._meta.db_table not in connection.introspection.table_names():
                raise ValueError(
                    "Table `{table_name}` is missing in test database.".format(table_name=WithTextModel._meta.db_table)
                )

    def tearDown(self):
        super().tearDown()

        with connection.schema_editor() as schema_editor:
            schema_editor.delete_model(WithTextModel)
