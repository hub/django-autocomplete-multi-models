import logging

from django.conf import settings
from django.core import management
from django.test import TestCase, override_settings

from autocomplete_multi_models import business_process, models, utils, signals
from autocomplete_multi_models.tests import test_helpers

logger = logging.getLogger(__name__)


class AutoCompleteTestCase(TestCase):
    def test_count(self):
        business_process.add_text_to_index("nous nous sommes promené")
        self.assertDictEqual(
            dict([(o.word, o.occurrence) for o in models.IndexedWord.objects.all()]),
            {
                'nous': 2,
                'sommes': 1,
                'promené': 1,
            },
        )
        business_process.add_text_to_index("nous")
        self.assertEqual(models.IndexedWord.objects.get(word='nous').occurrence, 3)
        business_process.add_text_to_index("test")
        self.assertEqual(models.IndexedWord.objects.get(word='test').occurrence, 1)

    def test_count_case(self):
        business_process.add_text_to_index("Nous nous sommes promené, et nous soMMes rentrés")
        self.assertDictEqual(
            dict([(o.word, o.occurrence) for o in models.IndexedWord.objects.all()]),
            {
                'Nous': 3,
                'sommes': 2,
                'promené': 1,
                'rentrés': 1,
            },
        )

    def test_count_case_accent(self):
        business_process.add_text_to_index("Nous nous sommes promené, et nous soMMes rentrés; nous sommés là")
        self.assertDictEqual(
            dict([(o.word, o.occurrence) for o in models.IndexedWord.objects.all()]),
            {
                'Nous': 4,
                'sommes': 3,
                'promené': 1,
                'rentrés': 1,
            },
        )

    def test_unaccent_ok(self):
        business_process.add_text_to_index("azertyêazerty azertyaezerty")
        self.assertEqual(models.IndexedWord.objects.count(), 2)

    def test_split_ok(self):
        business_process.add_text_to_index("abcd (abcd) abcd|abcd,,,,]]]abcd")
        self.assertEqual(models.IndexedWord.objects.count(), 1)

    def test_case_ignored(self):
        business_process.add_text_to_index("Nous nous")
        self.assertEqual(models.IndexedWord.objects.count(), 1)

    def test_init_from_settings_fails(self):
        settings.AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER = (
            "autocomplete_multi_models.tests.settings_storage_file_based.get_fcn",
            "autocomplete_multi_models.tests.settings_storage_file_based.get_fcn",
        )
        self.assertRaises(AssertionError, utils.init_from_settings)
        settings.AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER = (
            "autocomplete_multi_models.tests.settings_storage_file_based.get_fcn",
            "autocomplete_multi_models.tests.settings_storage_file_based.get_data",
        )
        self.assertRaises(TypeError, utils.init_from_settings)
        settings.AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER = (
            "autocomplete_multi_models.tests.settings_storage_file_based.get_fcn",
            "autocomplete_multi_models.tests.settings_storage_file_based.save_data",
        )
        self.assertRaises(TypeError, utils.init_from_settings)
        settings.AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER = (
            "autocomplete_multi_models.tests.settings_storage_file_based.get_fcn",
            "autocomplete_multi_models.tests.settings_storage_file_based.blabla",
        )
        self.assertRaises(ImportError, utils.init_from_settings)
        settings.AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER = None

    def test_accent_are_ignored(self):
        models.IndexedWord.objects.create(word="ARNtoto")
        models.IndexedWord.objects.create(word="ÄRNtoto")
        models.IndexedWord.objects.create(word="RNtoto")

        qs = business_process.get_closest_matching_words("ARN", limit=-1, min_similarity=-1)
        self.assertEqual(qs.get(word="ARNtoto").similarity, qs.get(word="ÄRNtoto").similarity)

        qs = business_process.get_closest_matching_words("ÄRN", limit=-1, min_similarity=-1)
        self.assertEqual(qs.get(word="ARNtoto").similarity, qs.get(word="ÄRNtoto").similarity)

        qs = business_process.get_closest_matching_words("ÄRNtoto", limit=-1, min_similarity=-1)
        self.assertGreater(qs.get(word="ARNtoto").similarity, qs.get(word="RNtoto").similarity)

    def test_search_without_accent_find_accent(self):
        models.IndexedWord.objects.create(word="azerty")
        models.IndexedWord.objects.create(word="azérty")

        qs = business_process.get_closest_matching_words("azerty", limit=-1, min_similarity=-1)
        self.assertEqual(qs.get(word="azérty").similarity, qs.get(word="azerty").similarity)


@override_settings(AUTOCOMPLETE_MIN_LENGTH=1)
class MinLength1(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.add_text_to_index("z bb ccc ddd")
        self.assertEqual(models.IndexedWord.objects.count(), 4)


@override_settings(AUTOCOMPLETE_MIN_LENGTH=2)
class MinLength2(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.add_text_to_index("z bb ccc ddd")
        self.assertEqual(models.IndexedWord.objects.count(), 3)


@override_settings(AUTOCOMPLETE_MIN_LENGTH=3)
class MinLength3(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.add_text_to_index("z bb ccc ddd")
        self.assertEqual(models.IndexedWord.objects.count(), 2)


@override_settings(AUTOCOMPLETE_MIN_LENGTH=1)
class BannedWord(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.add_text_to_index("a A b c d")
        self.assertEqual(models.IndexedWord.objects.count(), 3)


@override_settings(AUTOCOMPLETE_MIN_LENGTH=1)
class SubIntNotAdded(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.add_text_to_index("00001101 AAAA ZZZZ EEEE")
        self.assertEqual(models.IndexedWord.objects.count(), 3)


@override_settings(AUTOCOMPLETE_MIN_LENGTH=1)
class CompleteIntAdded(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.add_text_to_index("0000110")
        self.assertEqual(models.IndexedWord.objects.count(), 1)


@override_settings(AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER=None)
class NeedRebuildDefaultBehaviorTestCase(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        self.assertTrue(business_process.get_setting_from_storage(utils.REBUILD_NEEDED, True))
        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, False)
        self.assertFalse(business_process.get_setting_from_storage(utils.REBUILD_NEEDED, None))
        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, True)
        self.assertTrue(business_process.get_setting_from_storage(utils.REBUILD_NEEDED, None))
        management.call_command('makeautocompleteindex', no_fill_if_empty=True)
        self.assertFalse(business_process.get_setting_from_storage(utils.REBUILD_NEEDED, None))
        self.assertEqual(models.IndexedWord.objects.count(), 0)


@override_settings(SHOULD_MERGE_PLURAL_INTO_SINGULAR=True)
class MergePlural1(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.add_text_to_index("Gene genes gene Genes")
        self.assertEqual(models.IndexedWord.objects.count(), 1)


@override_settings(SHOULD_MERGE_PLURAL_INTO_SINGULAR=False)
class MergePlural2(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.add_text_to_index("Gene genes gene genes")
        self.assertEqual(models.IndexedWord.objects.count(), 2)


@override_settings(
    AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER=(
        "autocomplete_multi_models.tests.settings_storage_file_based.get_fcn",
        "autocomplete_multi_models.tests.settings_storage_file_based.set_fcn",
    ),
)
class NeedRebuildFileSettingsBasedTestCase(test_helpers.ChangeAutoCompleteSettingsTestCase):
    def test_it(self):
        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, "eeeeee")
        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, False)
        self.assertFalse(business_process.get_setting_from_storage(utils.REBUILD_NEEDED, True))
        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, True)

        models.IndexedWord.objects.create(word="TOTO")
        self.assertTrue(business_process.get_setting_from_storage(utils.REBUILD_NEEDED, True))
        management.call_command('makeautocompleteindex', no_fill_if_empty=True)
        self.assertFalse(business_process.get_setting_from_storage(utils.REBUILD_NEEDED, False))
        self.assertEqual(models.IndexedWord.objects.count(), 0)

        models.IndexedWord.objects.create(word="TOTO")
        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, False)
        management.call_command('makeautocompleteindex')
        self.assertEqual(models.IndexedWord.objects.count(), 1)

        business_process.set_setting_in_storage(utils.REBUILD_NEEDED, False)
        management.call_command('makeautocompleteindex', forced=True, no_fill_if_empty=True)
        self.assertEqual(models.IndexedWord.objects.count(), 0)

        signals.instance_delete(sender=None, instance=None, field_names=[])
        self.assertTrue(business_process.get_setting_from_storage(utils.REBUILD_NEEDED, True))


class WithTextModelTestCase(test_helpers.WithTextModelTestCase):
    def test_unmanaged_model(self):
        with self.assertNumQueries(num=3):
            self.assertEqual(0, test_helpers.WithTextModel.objects.all().count())
            test_helpers.WithTextModel.objects.create(text="bla")
            self.assertEqual(1, test_helpers.WithTextModel.objects.all().count())

    def test_signals_instance_update(self):
        o = test_helpers.WithTextModel.objects.create(text="poney")
        self.assertEqual(0, models.IndexedWord.objects.count())
        signals.instance_update(sender=test_helpers.WithTextModel, instance=o, field_names=["text"])
        self.assertEqual(1, models.IndexedWord.objects.count())

        signals.instance_delete(sender=test_helpers.WithTextModel, instance=o, field_names=["text"])
        self.assertEqual(1, models.IndexedWord.objects.count())

    def test_conditional_indexing(self):
        # there is a function indicating that NONONO should be not indexed
        test_helpers.WithTextModel.objects.create(text="NONONO")
        test_helpers.WithTextModel.objects.create(text="YESYES")
        for o in test_helpers.WithTextModel.objects.all():
            signals.instance_update(sender=test_helpers.WithTextModel, instance=o, field_names=["text"])
        self.assertFalse(models.IndexedWord.objects.filter(word="NONONO"), "Prevented by class implementation")
        self.assertTrue(models.IndexedWord.objects.filter(word="YESYES"), "Prevented by class implementation")

    def test_absence_of_conditional_indexing(self):
        # we remove the function, all instance should be indexed
        test_helpers.WithTextModel.objects.create(text="NONONO")
        test_helpers.WithTextModel.objects.create(text="YESYES")
        for o in test_helpers.WithTextModel.objects.all():
            setattr(o, utils.DEFAULT_CAN_BE_INDEXED_BY_AUTOCOMPLETE_FUNCTION_NAME, None)
            signals.instance_update(sender=test_helpers.WithTextModel, instance=o, field_names=["text"])
        self.assertTrue(models.IndexedWord.objects.filter(word="NONONO"), "Prevented by class implementation")
        self.assertTrue(models.IndexedWord.objects.filter(word="YESYES"), "Prevented by class implementation")
