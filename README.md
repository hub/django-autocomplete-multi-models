# django-autocomplete-multi-models

An app that index word used in text field across multiple models, and expose an api to query for word similar to the query.

The app use django signals to be notified when an instance is changed and update its index with the new values.

Quick start
-----------

1. Add "autocomplete_multi_models" at the end of your INSTALLED_APPS:

```py
    INSTALLED_APPS = [
        ...
        'autocomplete_multi_models',
    ]
```

2. Choose models and fields that you want to index:

```py
    AUTOCOMPLETE_TARGET_FIELDS = [
        ("my_app", "FirstModel", "my_text_field"),
        ("my_app", "FirstModel", "my_char_field"),
        ("my_app", "SecondModel", "my_char_field"),
        ... 
    ]
```

3. Adjust settings 

```py
    AUTOCOMPLETE_MIN_LENGTH = 4
    AUTOCOMPLETE_MIN_SIMILARITY = 0.3
    AUTOCOMPLETE_LIMIT = 10
    CAN_BE_INDEXED_BY_AUTOCOMPLETE_FUNCTION_NAME = "can_be_indexed_by_autocomplete"
    AUTOCOMPLETE_PERSISTENT_VARIABLE_GETTER_SETTER = (
        "my_app.blabla.get_fcn",
        "my_app.blabla.set_fcn",
    )
```

4. If you want to use rest_framework for querying words, include the URLconf in your project urls.py like this:

```py
    path('api/', include('autocomplete_multi_models.expose_with.rest_framework.urls')),
```

5. If you want to use graphene for querying words, Inherite the Query in your project schema.py like this:
    
```py
    class Query(
        my_app.schema.Query,
        autocomplete_multi_models.expose_with.graphene.schema.FindClosestWordsQuery,
        graphene.ObjectType,
    ):
        debug = graphene.Field(DjangoDebug, name="_debug")
```

6. To build the index, run the following commande. It is recommended to run it daily

```sh
    python manager.py makeautocompleteindex
```

