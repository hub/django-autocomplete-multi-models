#!/usr/bin/env python3
import os
import sys

import django
from decouple import config
from django.conf import settings
from django.test.utils import get_runner

BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "autocomplete_multi_models"))
if __name__ == "__main__":
    settings.configure(
        BASE_DIR=BASE_DIR,
        DEBUG=True,
        DATABASES={
            "default": {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": config("POSTGRES_DBNAME", default="postgres"),
                "USER": config("POSTGRES_USER", default="postgres"),
                "PORT": config("POSTGRES_PORT", default=5432),
                "PASSWORD": config("POSTGRES_PASSWORD", "test"),
                "HOST": config("POSTGRES_HOST", "db-local"),
            }
        },
        INSTALLED_APPS=("autocomplete_multi_models",),
        AUTOCOMPLETE_TARGET_FIELDS=[],
    )
    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    failures = test_runner.run_tests(["autocomplete_multi_models.tests"])
    sys.exit(bool(failures))
